import csv
from sklearn import model_selection
from typing import Tuple
from apyori import apriori


class RecommendationManager:
    """This class facilitates the generation of recommendation rules from datasets and the creation of recommendations from recommendation rules.
    
    Attributes
    ----------
    DATA : list
        list of loaded data set, one should not mutate this property
    TRAINING_DATA : list
        list of training data set, which is a result of the split_training_and_testing_data() function, one should not mutate this property
    TESTING_DATA : list
        list of testing data set, which is a result of the split_training_and_testing_data() function, one should not mutate this property
    RELATIONS : list
        list of relations, which is a result of the find_relations() function, one should not mutate this property
    RECOMMENDATION_RULES : list
        list of recommendation rules, which is a result of the make_recommendation_rules() function, one should not mutate this property
    """
    
    def __init__(self,
                 data_file_name: str,
                 random_state: float = 1,
                 test_size: float = 0.1,
                 min_support: float = 0.0045,
                 min_confidence: float = 0.2,
                 min_lift: float = 1.5):
        """Initialize the RecommendationManager
        
        Parameters
        ----------
        data_file_name : str
            The file name or the path to the data source
        random_state : float, optional
            Seed for the randomization of data set splitting process, by default 1
        test_size : float, optional
            Percentange of test data size. Possible value is 0 to 1, where 0 is 0% and 1 is 100%, by default 0.1
        min_support : float, optional
            Minimum support value for the apiori association rule function, by default 0.0045
        min_confidence : float, optional
            Minimum confidence value for the apiori association rule function, by default 0.2
        min_lift : float, optional
            Minimum lift value for the apiori association rule function, by default 1.5
        """

        self.DATA = self.load_data(data_file_name)
        self.TRAINING_DATA, self.TESTING_DATA = self.split_training_and_testing_data(
            self.DATA, random_state, test_size)
        self.RELATIONS = self.find_relations(self.TRAINING_DATA, min_support,
                                             min_confidence, min_lift)
        self.RECOMMENDATION_RULES = self.make_recommendation_rules(
            self.RELATIONS)

    @staticmethod
    def load_data(data_file_name: str) -> list:
        """Load data from CSV file
        
        Parameters
        ----------
        data_file_name : str
            The name of or path to the file to load data from
        
        Returns
        -------
        list
            Loaded CSV data
        """

        data = []
        with open(data_file_name) as data_file:
            csv_reader = csv.reader(data_file, skipinitialspace=True)
            for row in csv_reader:
                data.append(row)
        return data

    @staticmethod
    def split_training_and_testing_data(data: list, random_state: float,
                                        test_size: float) -> Tuple[list, list]:
        """Split the data into training and testing set
        
        Parameters
        ----------
        data : list
            The initial data set
        random_state : float
            Seed for the randomization of data set splitting process
        test_size : float
            Percentange of test data size. Possible value is 0 to 1, where 0 is 0% and 1 is 100%. (default: {0.1})  
        
        Returns
        -------
        Tuple[list, list]
            A tuple of two lists, with the first list being the TRAINING data set, and the second list being the TESTING data set
        """

        training_data, testing_data = model_selection.train_test_split(
            data, random_state=random_state, test_size=test_size)
        return (training_data, testing_data)

    @staticmethod
    def find_relations(data: list, min_support: float, min_confidence: float,
                       min_lift: float) -> list:
        """[summary]
        
        Parameters
        ----------
        data : list
            The dataset to find relations
        min_support : float
            Minimum support value for the apiori association rule function
        min_confidence : float
            Minimum confidence value for the apiori association rule function
        min_lift : float
            Minimum lift value for the apiori association rule function
        
        Returns
        -------
        list
            A list of relationship
        """

        relations = apriori(data,
                            min_support=min_support,
                            min_confidence=min_confidence,
                            min_lift=min_lift)
        return list(relations)

    @staticmethod
    def make_recommendation_rules(relations: list) -> list:
        """Make a list of recommendation rules from a list of relations
        
        Parameters
        ----------
        relations : list
            A list of relations that will be used to create the recommendation rules
        
        Returns
        -------
        list
            A list of recommendation rules
        """
        rules = []
        for relation in relations:
            stat = relation.ordered_statistics[0]
            rules.append({
                'items': relation.items,
                'items_base': stat.items_base,
                'items_add': stat.items_add,
                'lift': stat.lift
            })
        return rules

    def get_recommendations(self, items_in_cart: list) -> list:
        """Get list of recommended items for the supplied items in cart
        
        Parameters
        ----------
        items_in_cart : list
            Items already in the cart
        
        Returns
        -------
        list
            List of dicts, whereas within each dict the, 'item' being the name of the recommended item
            and 'strength' being the strength of the recommendation.
        """
        recommendations = []
        items = frozenset(items_in_cart)
        rules = self.RECOMMENDATION_RULES
        for rule in rules:
            # !! Set operation:
            # !! 'if set_A <= set_B' means 'if set_A is a subset of set_B'
            if rule['items_base'] <= items:
                recommendations.append({
                    'item': list(rule['items_add'])[0],
                    'strength': rule['lift']
                })
        return sorted(recommendations,
                      key=lambda recommendation: recommendation['strength'],
                      reverse=True)


# if __name__ == "__main__":
#     recommendation_manager = RecommendationManager('store_data.csv')
#     recommended_items = recommendation_manager.get_recommendations(
#         ['mineral water', 'butter'])
#     print(recommended_items)
