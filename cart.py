class CartManager:
    """
    Cart manager
    This class use to store owner's first name, last name and items.
    """
    
    def __init__(self, first_name: str, last_name: str):
        """
        Initialize (Constructor).
        Arguments:
            first_name -- A first name of owner (eg. 'Benjapol', 'Kasidit' )
            last_name -- A last name of owner (eg. 'Worakan', 'Phoncharoen' )
        """
        # Create a initialize variable that store first name(String), last name(String) and items(Dict)
        # Your code go here

        self.FIRST_NAME = first_name
        self.LAST_NAME = last_name
        self._items = {}

    def add_item(self,item: str, amount: int):
        """
        Add a item to items.
        Arguments:
            item(String) -- An item as string (eg. 'egg', 'milk').
            amount(int)  -- An amount of item (eg. 3, 2)
        """ 
        # Add item and amount to items and store as Dict
        # (eg. 
        #   case1: if item not exist in items
        #   input: item = 'milk', amount = 5
        #   result {'milk': 5}
        #   case2: if item exist in items
        #   input: item = 'milk', amount = 5
        #   result: add amount to existing item {'milk': 5}
        # )
        # Your code go here
        if self._items.get(item):
            self._items[item]=self._items[item]+amount
            
        else:
            self._items[item]=amount
        # raise NotImplementedError

    def remove_item(self,item):
        """
        Remove a item from items.
        Arguments:
            item -- An item as string (eg. 'egg', 'milk').
        """
        # Remove an item from items. HINT: use pop() (https://www.programiz.com/python-programming/methods/dictionary/pop)
        # (eg. 
        # Initializer: {'milk': 5}
        # Input: 'milk'
        # Result: {}
        # )
        # Your code go here
        if self._items.get(item,'no')=='no':
            print("maimee this item woi")
        else:
            self._items.pop(item)
        # raise NotImplementedError

    @property
    def total_amount_items(self) -> int:
        """
        Returns total number of items in cart
        """
        # Return the total number of item in items.
        # (eg. 
        # Initializer: {'milk': 5, 'egg': 10}
        # Return 15
        # Your code go here
        # raise NotImplementedError
        total = 0 
        for item in self._items.values():
            total += item
        print(total)
        return total


    @property
    def owner_name(self) -> dict:
        """
        Return owner's first name and last name in cart as Dict
        """
        # Return owner's first name and last name in cart as Dict
        # (eg. 
        # Initializer: first_name = 'Kasidit', last_name = 'Phoncharoen'
        # Return {'first_name': 'Kasidit', 'last_name': 'Phoncharoen'}
        # Your code go here
        return {'first_name': self.FIRST_NAME, 'last_name': self.LAST_NAME}

    @property
    def items(self) -> dict:
        """
        Return items in cart
        """
        # Return the items in cart of that items.
        # (eg. 
        # Initializer: {'milk': 5, 'egg': 10}
        # Return {'milk': 5, 'egg': 10}
        # Your code go here
        # raise NotImplementedError
        return self._items

if __name__ == "__main__":
    cart = CartManager("a","b")
    cart.add_item("dog",5)
    print(cart._items)
    cart.add_item("cat",10)
    print(cart._items)
    cart.remove_item("dg")
    print(cart._items)
    # cart.remove_item("cat")
    # print(cart.items)
    a=cart.total_amount_items
    print(a)
    b=cart.items
    print(b)